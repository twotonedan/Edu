'use strict';

// Declare app level module which depends on views, and components
var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(['$routeProvider',
    function($routeProvider) {

        $routeProvider
            .when('/home', {
                templateUrl: 'pages/home.html',
                controller: 'mainController'
            })
            .when('/view1', {
                templateUrl: 'pages/view1/view1.html',
                controller: 'view1Controller'
            })            
            .when('/view2', {
                templateUrl: 'pages/view2/view2.html',
                controller: 'view2Controller'
            })
            .otherwise({
                redirectTo: '/home'
            });
    }
]);

myApp.controller('mainController', function($scope) {

  $scope.message = 'Nada';
});